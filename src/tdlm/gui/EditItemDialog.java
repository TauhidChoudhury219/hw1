/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdlm.gui;

import java.time.LocalDate;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import tdlm.data.ToDoItem;

/**
 *
 * @author Tchou808
 */



    import java.time.LocalDate;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import tdlm.PropertyType;
import tdlm.controller.ToDoListController;
import tdlm.data.DataManager;
import tdlm.data.ToDoItem;

import tdlm.controller.ToDoListController;
import tdlm.data.DataManager;
import saf.ui.AppYesNoCancelDialogSingleton;
import saf.ui.AppMessageDialogSingleton;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import tdlm.PropertyType;
import tdlm.data.ToDoItem;






public class EditItemDialog{
    Stage place;
    
    VBox layout1;
    HBox categoryPane;
    TextField categoryText;
    HBox descriptionPane;
    TextField descriptionText;
    HBox startDatePane;
    DatePicker startText;
    HBox endDatePane;
    DatePicker endText;
    HBox completedPane;
    TextField completedText;
    Button ok;
    Button cancel;
    HBox buttonPane;
    
    VBox layout2;
    HBox whole;
    VBox complete;
    public String selection="X";
    
    public EditItemDialog(){
	
	   layout2=new VBox();
	   whole=new HBox();
	   complete=new VBox();
	   layout1= new VBox();
	   
	  PropertiesManager props = PropertiesManager.getPropertiesManager();
        
	   place = new Stage(); 
	   place.initModality(Modality.APPLICATION_MODAL);
	   
	   layout1= new VBox();
	   Label HELLO_LABEL= new Label();
	   HELLO_LABEL.setText(props.getProperty(PropertyType.HELLO_LABEL));
	   
	   categoryPane= new HBox();
	   Label CATEGORY_LABEL = new Label();
	   CATEGORY_LABEL.setText(props.getProperty(PropertyType.CATEGORY_COLUMN_HEADING));
	   categoryText= new TextField();
	  // categoryPane.getChildren().addAll(CATEGORY_LABEL, categoryText);
	   
	   
	   descriptionPane= new HBox();
	   Label DESCRIPTION_LABEL= new Label ();
	   DESCRIPTION_LABEL.setText(props.getProperty(PropertyType.DESCRIPTION_COLUMN_HEADING));
	   descriptionText= new TextField();
	//   descriptionPane.getChildren().addAll(DESCRIPTION_LABEL,descriptionText);
	   
	    
	   startDatePane= new HBox();
	   Label START_DATE_LABEL= new Label ();
	    START_DATE_LABEL.setText(props.getProperty(PropertyType.START_DATE_COLUMN_HEADING));
	   startText= new DatePicker();
	  // startDatePane.getChildren().addAll(START_DATE_LABEL, startText);
	   
	   
	   endDatePane= new HBox();
	   Label END_DATE_LABEL= new Label ();
	   END_DATE_LABEL.setText(props.getProperty(PropertyType.END_DATE_COLUMN_HEADING));
	   endText= new DatePicker();
	 //  endDatePane.getChildren().addAll(END_DATE_LABEL, endText);
	   
	   completedPane= new HBox();
	   Label COMPLETED_LABEL= new Label ();
	   COMPLETED_LABEL.setText(props.getProperty(PropertyType.COMPLETED_COLUMN_HEADING));
	   completedText= new TextField();
	//   completedPane.getChildren().addAll(COMPLETED_LABEL, completedText);
	   
	   
	   ok= new Button("Ok");
	   cancel= new Button("Cancel");
	   buttonPane= new HBox(ok, cancel);
	   
	      
		

layout1.getChildren().addAll(CATEGORY_LABEL, DESCRIPTION_LABEL, START_DATE_LABEL, END_DATE_LABEL, COMPLETED_LABEL);
		layout2.getChildren().addAll(categoryText, descriptionText, startText, endText, completedText );
		
		layout1.setMinWidth(50);
		layout2.setSpacing(50);
		whole.getChildren().addAll(layout1, layout2);
		
		whole.setSpacing(50);
		complete.getChildren().addAll(HELLO_LABEL, whole, buttonPane);
		complete.setPadding(new Insets(100, 80, 100, 80));
		
		
		//layout1.getChildren().addAll(HELLO_LABEL, categoryPane, descriptionPane, startDatePane, endDatePane, completedPane, buttonPane);
	   
	   
	   
	   Scene dialogScene = new Scene(complete);
	   place.setScene(dialogScene);
    }
    public void show(ToDoItem newItem){
	
	categoryText.setText(newItem.getCategory());
	descriptionText.setText(newItem.getDescription());
	startText.setValue(newItem.getStartDate());
	endText.setValue(newItem.getEndDate());
	
	
	
	ok.setOnAction(e->{
	 
	   
	   String category= categoryText.getText();
	   String description= descriptionText.getText();
	   LocalDate startDate= startText.getValue();
	   LocalDate endDate= endText.getValue();
	   boolean completed= true;
	
	   
	   newItem.setCategory(category);
	   newItem.setDescription(description);
	   newItem.setStartDate(startDate);
	   newItem.setEndDate(endDate);
	   newItem.setCompleted(completed);
	   
	   place.close();
	   
	});
	
	
	place.showAndWait();
	
    }
}
