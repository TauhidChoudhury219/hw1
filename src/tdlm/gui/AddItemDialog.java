/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdlm.gui;

import java.time.LocalDate;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import tdlm.controller.ToDoListController;
import tdlm.data.DataManager;
import tdlm.data.ToDoItem;

/**
 *
 * @author Tchou808
 */
public class AddItemDialog {
    Stage place;
    
    VBox layout1;
    HBox categoryPane;
    TextField categoryText;
    HBox descriptionPane;
    TextField descriptionText;
    HBox startDatePane;
    DatePicker startText;
    HBox endDatePane;
    DatePicker endText;
    HBox completedPane;
    TextField completedText;
    Button ok;
    Button cancel;
    HBox buttonPane;
 
    VBox layout2;
    HBox whole;
    VBox complete;
    public String selection="X";
    public AddItemDialog(){
	   place = new Stage(); 
	   place.initModality(Modality.APPLICATION_MODAL);
	   
	
	   layout2=new VBox();
	   whole=new HBox();
	   complete=new VBox();
	   layout1= new VBox();
	   Label hello= new Label("Please Enter Information:");
	   hello.setFont(Font.font(25));
	   
	   categoryPane= new HBox();
	   Label categoryLabel = new Label("Category");
	   categoryText= new TextField();
	//   categoryPane.getChildren().addAll(categoryLabel, categoryText);
	   
	   
	   descriptionPane= new HBox();
	   Label descriptionLabel= new Label ("Description");
	   descriptionText= new TextField();
	//   descriptionPane.getChildren().addAll(descriptionLabel,descriptionText);
	   
	    
	   startDatePane= new HBox();
	   Label StartDateLabel= new Label ("Start Date");
	   startText= new DatePicker();
	  // startText.setMaxWidth(150);
	  // StartDateLabel.setMinWidth(50);
	  // startDatePane.getChildren().addAll(StartDateLabel, startText);
	   
	   
	   endDatePane= new HBox();
	   Label EndDateLabel= new Label ("End Date");
	   endText= new DatePicker();
	  // endText.setMaxWidth(150);
//	   endDatePane.getChildren().addAll(EndDateLabel, endText);
	   
	   completedPane= new HBox();
	   Label completedLabel= new Label ("Completed");
	   completedText= new TextField();
//	   completedPane.getChildren().addAll(completedLabel, completedText);
	   
	   
	   ok= new Button("Ok");
	   cancel= new Button("Cancel");
	   buttonPane= new HBox(ok, cancel);
	   
	        
		/*layout1.setPadding(new Insets(80, 60, 80, 60));
		layout1.setSpacing(20);
		categoryPane.setSpacing(190);
		descriptionPane.setSpacing(190);
		startDatePane.setSpacing(190);
		endDatePane.setSpacing(190);
		completedPane.setSpacing(190);
		*/
		
		
		layout1.getChildren().addAll(categoryLabel, descriptionLabel, StartDateLabel, EndDateLabel, completedLabel);
		layout2.getChildren().addAll(categoryText, descriptionText, startText, endText, completedText );
		whole.getChildren().addAll(layout1, layout2);
		
		whole.setSpacing(50);
		complete.getChildren().addAll(hello, whole, buttonPane);
		complete.setPadding(new Insets(80, 60, 80, 60));
	   Scene dialogScene = new Scene(complete);
	   place.setScene(dialogScene);
    }
    public void show(ToDoItem newItem){
	ok.setOnAction(e->{
	 
	   selection="OK";
	   String category= categoryText.getText();
	   String description= descriptionText.getText();
	   LocalDate startDate= startText.getValue();
	   LocalDate endDate= endText.getValue();
	   boolean completed= true;
	
	   
	   newItem.setCategory(category);
	   newItem.setDescription(description);
	   newItem.setStartDate(startDate);
	   newItem.setEndDate(endDate);
	   newItem.setCompleted(completed);
	   
	   
	   
	   place.close();
	   
	});
	
	cancel.setOnAction(e->{
    
	    selection="Cancel";
       place.close();
    
    });
	
	place.showAndWait();
	
    }
}
