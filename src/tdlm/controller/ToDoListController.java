package tdlm.controller;

import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.TableView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javax.imageio.ImageIO;
import tdlm.data.DataManager;
import tdlm.gui.Workspace;
import saf.AppTemplate;
import saf.ui.AppYesNoCancelDialogSingleton;
import tdlm.data.ToDoItem;
import tdlm.gui.AddItemDialog;
import tdlm.gui.EditItemDialog;

/**
 * This class responds to interactions with todo list editing controls.
 * 
 * @author McKillaGorilla
 * @version 1.0
 */
public class ToDoListController {
    AppTemplate app;
    
    public ToDoListController(AppTemplate initApp) {
	app = initApp;
    }
    
    public void processAddItem() {	
	// ENABLE/DISABLE THE PROPER BUTTONS 
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
	DataManager data= (DataManager)app.getDataComponent();
	workspace.reloadWorkspace();
	AddItemDialog dialog= new AddItemDialog();
	ToDoItem newItem = new ToDoItem();
	dialog.show(newItem);
	if(dialog.selection.equals("OK")){
	    data.addItem(newItem);
	}
app.getGUI().updateToolbarControls(false);

	
	
    }
    
    public void processRemoveItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	DataManager data= (DataManager)app.getDataComponent();
	
	AppYesNoCancelDialogSingleton dialog= AppYesNoCancelDialogSingleton.getSingleton();
	    dialog.show("Warning", "Are you sure you want to remove the highlighted field?");
	    System.out.println(dialog.getSelection());
	   
	TableView itemTable= workspace.getTable();
	ToDoItem removeItem= (ToDoItem) itemTable.getSelectionModel().getSelectedItem();
	
	if(dialog.getSelection().equals("Yes")&& removeItem!=null){
	    data.getItems().remove(removeItem);
	}
	app.getGUI().updateToolbarControls(false);
	
    }
    
    public void processMoveUpItem() {
	
	
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	DataManager data= (DataManager)app.getDataComponent();
		
		
	    
	    	TableView itemTable= workspace.getTable();
	
		ToDoItem moveUp= (ToDoItem) itemTable.getSelectionModel().getSelectedItem();
		int moveUpIndex=itemTable.getItems().indexOf(moveUp);
		
		
		
		if(moveUpIndex>0 ){
		int aboveMoveUpIndex= itemTable.getItems().indexOf(moveUp)-1;
		ToDoItem aboveMoveUp= (ToDoItem) itemTable.getItems().get(aboveMoveUpIndex);
		
		data.getItems().set(moveUpIndex, aboveMoveUp);
		data.getItems().set(aboveMoveUpIndex, moveUp);
		moveUpIndex--;
		}
		app.getGUI().updateToolbarControls(false);
	    
    }
    
    public void processMoveDownItem() {
		 Workspace workspace = (Workspace)app.getWorkspaceComponent();
		DataManager data= (DataManager)app.getDataComponent();
	    	TableView itemTable= workspace.getTable();
	
		ToDoItem moveDown= (ToDoItem) itemTable.getSelectionModel().getSelectedItem();
		int moveDownIndex=itemTable.getItems().indexOf(moveDown);
		
		
		
		if(moveDownIndex<(data.getItems().size()-1) ){
		int belowMoveDownIndex= itemTable.getItems().indexOf(moveDown)+1;
		ToDoItem belowMoveDown= (ToDoItem) itemTable.getItems().get(belowMoveDownIndex);
		data.getItems().set(belowMoveDownIndex, moveDown);
		data.getItems().set(moveDownIndex, belowMoveDown);
		moveDownIndex++;
		}
		
		
		app.getGUI().updateToolbarControls(false);
    }
    
    public void processEditItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	DataManager data= (DataManager)app.getDataComponent();
	TableView itemTable= workspace.getTable();
	
	ToDoItem edit= (ToDoItem) itemTable.getSelectionModel().getSelectedItem();
	EditItemDialog editor = new EditItemDialog();
	editor.show(edit);
	app.getGUI().updateToolbarControls(false);
    }
}
